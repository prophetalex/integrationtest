FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY ./ApiTest/bin/Debug .
ENV ASPNETCORE_URLS http://*:5000
ENV ASPNETCORE_ENVIRONMENT debug
EXPOSE 5000
ENTRYPOINT ["dotnet", "ApiTest.dll"]